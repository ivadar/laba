#include <QCoreApplication>
#include "Splay.h"

int main(int argc, char *argv[])
{
        Comparator<int> *cmp = new Default_comparator<int>();
        SplayTree<int, int> my_tree(cmp);
        Node<int, int> node1(1, 0), node2(2, 0), node3(3, 0),node4(4, 0),node5(5, 0),node6(6, 0), node7(7, 0), node11(11, 0);
//        my_tree.insert(node5);
        my_tree.insert(node1);
//        my_tree.insert(node6);
//        my_tree.insert(node3);
        my_tree.insert(node2);
        my_tree.insert(node4);
//        my_tree.insert(node11);
        my_tree.insert(node7);
        my_tree.prefix(print_result<int, int>);
        std::cout << "------------------------------\n";
        std::cout << my_tree.find(1) << '\n';
        my_tree.prefix(print_result<int, int>);
        std::cout << "------------------------------\n";
        my_tree.remove(1) << '\n';
        my_tree.prefix(print_result<int, int>);
        std::cout << "end\n";
        delete cmp;
    return 0;
}
