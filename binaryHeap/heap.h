#ifndef HEAP_H
#define HEAP_H

#include <iostream>
#include <exception>
#include <string>
#include <set>

using namespace std;

template <typename T>

class Comparator
{
    public:
        virtual int compare(const T& object_1, const T& object_2) const = 0;
        virtual ~Comparator() = default;
};

template <typename T>

class Default_comparator: public Comparator<T>
{
    int compare(const T& object_1, const T& object_2) const
    {
        if (object_1 > object_2)
        {
            return 1;
        }
        if (object_1 < object_2)
        {
            return -1;
        }
        return 0;
    }
};

template <typename TKey, typename TValue>

struct Node
{
    TKey key;
    TValue value;
    Node *left = nullptr;
    Node *right = nullptr;
};

class HeapException: public std::exception
{
    private:
        string msg;
    public:
        HeapException()
        {

        };
        void setMsg(const string& msg)
        {
            this->msg = msg;
        }
        const char* what() const noexcept
        {
            return msg.c_str();
        }
};
class GetMinError: public HeapException
{
    public:
    GetMinError()
    {
        setMsg("[ERROR] : An attempt to take a value by the minimum key from an empty one pyramids!");
    };
};
class RemoveMinError: public HeapException
{
    public:
    RemoveMinError()
    {
        setMsg("[ERROR] : An attempt to remove a value by the minimum key from an empty one pyramids!");
    }
};
template <typename TKey>

class InsertError: public HeapException
{
    public:
    InsertError(const TKey& key)
    {
        cout << "Key: " << key;
        setMsg("[ERROR] : An attempt to insert a value by key, it is already inpyramid");
    }
};
template <typename TKey>

class MergeError: public HeapException
{
    public:
    MergeError(const TKey& key)
    {
        cout << "Key: " << key;
        setMsg("[ERROR] : An attempt to merge pyramids containing identical (by comparator) keys");
    }
};


template <typename TKey, typename TValue>

class Heap
{
    public:
        public:
        Comparator<TKey> *comparator = nullptr;
        Node<TKey, TValue> *heapPtr = nullptr;
        int heapSize = 0;
        set<TKey> setKey;
        virtual void insertValue(const TKey &key, const TValue &value) = 0;
        virtual void getMin(TValue& value) = 0;
        virtual void removeMinKey() = 0;
        virtual void merge(const Heap<TKey, TValue>& addHeap) = 0;
        virtual ~Heap()
        {
            //
        };
};

template <typename TKey, typename TValue>

class CreatorHeap
{
    public:
    virtual ~CreatorHeap() {};
    virtual Heap<TKey, TValue> *FactoryMethod(Comparator<TKey> *compare) const = 0;
};

#endif // HEAP_H
