#ifndef SPLAY_H
#define SPLAY_H
#include "binaryTree.h"
template <typename TKey, typename TValue>

Node<TKey, TValue>* splay(Node<TKey, TValue> *&root, Comparator<TKey> *keyComparator, std::stack<Node<TKey, TValue> *>& stackNode);

template <typename TKey, typename TValue>

class SplayTree: public BinaryTree<TKey, TValue>
{
    protected:
        class SplayInsert: public BinaryTree<TKey, TValue>::InsertTemplate
        {
            protected:
            void insertHook(Node<TKey, TValue> *& root,
                            const TKey &key, const TValue &value,
                            Comparator<TKey> *keyComparator, std::stack<Node<TKey, TValue> *>& stackNode)
            {
                splay(root, keyComparator, stackNode);
            }
        };
        class SplayFind: public BinaryTree<TKey, TValue>::FindTemplate
        {
            protected:
            void findHook(Node<TKey, TValue> *& root, const TKey &key, const TValue &value,
                        Comparator<TKey> *keyComparator, std::stack<Node<TKey, TValue> *>& stackNode)
            {
                splay(root, keyComparator, stackNode);
            }
        };
        class SplayRemove: public BinaryTree<TKey, TValue>::RemoveTemplate
        {
            protected:
            void removeHook(Node<TKey, TValue> *& root,
                            const TKey &key, const TValue &value,
                            Comparator<TKey> *keyComparator, std::stack<Node<TKey, TValue> *>& stackNode);

            void removenodeHook(Node<TKey, TValue> *&current, Node<TKey, TValue> *&parent,
                                            TValue &value, std::stack<Node<TKey, TValue> *>& stackNode)
            {
                stackNode.push(current);
                value = current->value;
            }
        };
    public:
    SplayTree(Comparator<TKey> *compare)
    {
        this->comparator = compare;
        this->inserter = new SplayInsert;
        this->finder = new SplayFind;
        this->remover = new SplayRemove;
    };
    ~SplayTree()
    {
        delete this->inserter;
        delete this->remover;
        delete this->finder;
    };
};

template <typename TKey, typename TValue>

Node<TKey, TValue>* zig(Node<TKey, TValue> *p, Node<TKey, TValue> *c)
{
    if (p->right == c) // левый разворот
    {
        p->right = c->left;
        c->left = p;
    }
    else // правый разворот
    {
        p->left = c->right;
        c->right = p;
    }
    return c;
}

template <typename TKey, typename TValue>

Node<TKey, TValue>* zig_zig(Node<TKey, TValue> *g, int rotate)
{
    Node<TKey, TValue> *p, *c;
    if (rotate > 0) // правый-правый поворот
    {
        p = g->left;
        c = p->left;
        g->left = p->right;
        p->right = g;
        p->left = c->right;
        c->right = p;
    }
    else // левый-левый
    {
        p = g->right;
        c = p->right;
        g->right = p->left;
        p->left = g;
        p->right = c->left;
        c->left = p;
    }
    return c;
}

template <typename TKey, typename TValue>

Node<TKey, TValue>* zig_zag(Node<TKey, TValue> *g, int rotate)
{
    Node<TKey, TValue> *p, *c;
    if (rotate < 0) // правый-левый
    {
        p = g->right;
        c = p->left;
        p->left = c->right;
        c->right = p;
        g->right = c->left;
        c->left = g;
    }
    else // левый-правый
    {
        p = g->left;
        c = p->right;
        p->right = c->left;
        c->left = p;
        g->left = c->right;
        c->right = g;
    }
    return c;
}

template <typename TKey, typename TValue>

Node<TKey, TValue>* splay(Node<TKey, TValue> *&root, Comparator<TKey> *keyComparator, std::stack<Node<TKey, TValue> *>& stackNode)
{
    Node<TKey, TValue> *nodecur, *nodeParent, *nodeGrand, *x;
    if(stackNode.empty())
    {
        return root;
    }
    else if (stackNode.top() == root)
    {
        return root;
    }
    nodecur = stackNode.top();
    stackNode.pop();
    while(!stackNode.empty())
    {
        nodeParent = stackNode.top();
        if (nodeParent == root)
        {
            root = zig(nodeParent, nodecur);
            return root;
        }
        stackNode.pop();
        nodeGrand = stackNode.top();
        if ((nodeGrand->left == nodeParent && nodeParent->left == nodecur) || (nodeGrand->right == nodeParent && nodeParent->right == nodecur))
        {
            x = zig_zig(nodeGrand, keyComparator->compare(nodeGrand->key, nodecur->key));
        }
        else if ((nodeGrand->left == nodeParent && nodeParent->right == nodecur) || nodeGrand->right == nodeParent && nodeParent->left == nodecur)
        {
            x = zig_zag(nodeGrand, keyComparator->compare(nodeGrand->key, nodecur->key));
        }
        stackNode.pop();
        if (!stackNode.empty())
        {
            if (static_cast<Node<TKey, TValue> *>(stackNode.top())->right == nodeGrand)
            {
                static_cast<Node<TKey, TValue> *>(stackNode.top())->right = x;
            }
            else
            {
                static_cast<Node<TKey, TValue> *>(stackNode.top())->left = x;
            }
        }
        else
        {
            root = x;
            return root;
        }
    }
    return root;
}

template <typename TKey, typename TValue>

void split(Node<TKey, TValue> *&root, Node<TKey, TValue> *&treeLeft, Node<TKey, TValue> *&treeRight)
{
    if (!root)
    {
        treeLeft = nullptr;
        treeRight = nullptr;
        return;
    }
    treeLeft = root->left;
    root->left = nullptr;
    treeRight = root;
    return;
}

template <typename TKey, typename TValue>

Node<TKey, TValue>* merge(Node<TKey, TValue> *treeLeft, Node<TKey, TValue> *treeRight, Comparator<TKey> *keyComparator)
{
    Node<TKey, TValue> *maxelem = treeLeft;
    std::stack<Node<TKey, TValue> *> stackNode;
    while(maxelem)
    {
        stackNode.push(maxelem);
        maxelem = maxelem->right;
    }
    splay(treeLeft, keyComparator, stackNode);
    if (treeLeft)
    {
        treeLeft->right = treeRight;
    }
    else
    {
        treeLeft = treeRight;
    }
    return treeLeft;
}

template <typename TKey, typename TValue>

void SplayTree<TKey, TValue>::SplayRemove::removeHook(Node<TKey, TValue> *& root, const TKey &key, const TValue &value,
                            Comparator<TKey> *keyComparator, std::stack<Node<TKey, TValue> *>& stackNode)
{
    Node<TKey, TValue> *treeLeft = nullptr, *treeRight = nullptr, *remove;
    splay(root, keyComparator, stackNode);
    split(root, treeLeft, treeRight);
    remove = treeRight;
    treeRight = treeRight->right;
    delete remove;
    remove = nullptr;
    root = merge(treeLeft, treeRight, keyComparator);
    return;
}
#endif // SPLAY_H
