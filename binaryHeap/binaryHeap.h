#ifndef BINARYHEAP_H
#define BINARYHEAP_H
#include "heap.h"

template <typename TKey, typename TValue>

class CreatorBinaryHeap;

template <typename TKey, typename TValue>

class BinaryHeap: public Heap<TKey, TValue>
{
    public:
        void insertValue(const TKey &key, const TValue &value);
        void getMin(TValue& value);
        void removeMinKey();
        void merge(const Heap<TKey, TValue>& addHeap);
        ~BinaryHeap();
        friend class CreatorBinaryHeap <TKey, TValue>;
    private:
        BinaryHeap(Comparator<TKey> *compare);
        void heapify(int i);
};

template <typename TKey, typename TValue>

class CreatorBinaryHeap: public CreatorHeap<TKey, TValue>
{
    public:
        Heap<TKey, TValue>* FactoryMethod(Comparator<TKey> *compare) const override
        {
            return new BinaryHeap<TKey, TValue>(compare);
        }
};

template <typename TKey, typename TValue>

BinaryHeap<TKey, TValue>::BinaryHeap(Comparator<TKey> *compare)
{
    this->comparator = compare;
    this->heapSize = 0;
}

template <typename TKey, typename TValue>

void BinaryHeap<TKey, TValue>::heapify(int i)
{
    int leftChild, rightChild, lessChild;
    leftChild = 2*i + 1;
    rightChild = 2*i + 2;
    lessChild = i;

    while(true)
    {
        int cmp = (this->comparator)->compare(this->heapPtr[lessChild].key, this->heapPtr[leftChild].key);
        if(leftChild < this->heapSize && cmp > 0)
        {
            lessChild = leftChild;
        }

        cmp = (this->comparator)->compare(this->heapPtr[lessChild].key, this->heapPtr[leftChild].key);
        if (rightChild < this->heapSize && cmp > 0)
        {
            lessChild = rightChild;
        }

        if (lessChild == i)
        {
            break;
        }
        swap(this->heapPtr[lessChild], this->heapPtr[i]);
        i = lessChild;
    }
}

template <typename TKey, typename TValue>

void BinaryHeap<TKey, TValue>::insertValue(const TKey &key, const TValue &value)
{
    Node<TKey, TValue> *newPtr = new Node<TKey, TValue>[this->heapSize + 1];
    this->setKey.insert(key);
    for (int i = 0; i < this->heapSize; i++)
    {
        newPtr[i] = this->heapPtr[i];
    }
    delete[] this->heapPtr;
    this->heapPtr = newPtr;
    newPtr = nullptr;

    int index = this->heapSize;
    this->heapPtr[index].key = key;
    this->heapPtr[index].value = value;

    int parent = (index - 1)/2;

    while(parent >= 0 && index > 0)
    {
        int cmp = (this->comparator)->compare(this->heapPtr[index].key, this->heapPtr[parent].key);
        if (cmp == 0)
        {
            throw InsertError<TKey>(this->heapPtr[index].key);
        }
        else if (cmp < 0)
        {
            swap(this->heapPtr[index], this->heapPtr[parent]);
        }
        index = parent;
        parent = (index - 1)/2;
    }
    this->heapSize += 1;
}

template <typename TKey, typename TValue>

void BinaryHeap<TKey, TValue>::getMin(TValue& value)
{
    if (!this->heapPtr)
    {
        throw GetMinError();
    }
    value = this->heapPtr[0].value;
}

template <typename TKey, typename TValue>

void BinaryHeap<TKey, TValue>::merge(const Heap<TKey, TValue>& addHeap)
{
    int i;
    try
    {
        for (i = 0; i < addHeap.heapSize; i++)
        {
            this->insertValue(addHeap.heapPtr[i].key, addHeap.heapPtr[i].value);
        }
    }
    catch(HeapException &e)
    {
        throw MergeError<TKey>(addHeap.heapPtr[i].key);
    }
}

template <typename TKey, typename TValue>

void BinaryHeap<TKey, TValue>::removeMinKey()
{
    if (!this->heapPtr)
    {
        throw RemoveMinError();
    }
    Node<TKey, TValue> removeNode;
    this->setKey.erase(this->heapPtr[0].key);
    removeNode = this->heapPtr[0];
    this->heapPtr[0] = this->heapPtr[this->heapSize - 1];
    this->heapSize -= 1;
    heapify(0);
}

template <typename TKey, typename TValue>

BinaryHeap<TKey, TValue>::~BinaryHeap()
{
    delete[] this->heapPtr;
    this->heapSize = 0;
}

#endif // BINARYHEAP_H
