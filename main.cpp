#include <iostream>
#include <string>
#include <stdarg.h>

std::string sum(std::string num1, std::string num2, int base)
{
    int len1 = num1.length();
    int len2 = num2.length();
    if (len1 > len2)
    {
        for (int i = 0; i <= len1 - len2; i++)
            num2 = '0' + num2;
        len2 = len1;
        num1 = '0' + num1;
    }
    else
    {
        for (int i = 0; i <= len2 - len1; i++)
            num1 = '0' + num1;
        len1 = len2;
        num2 = '0' + num2;
    }

    for (int i = len1; i > 0; i--)
    {
        int num = (isdigit(num1[i]) ? num1[i] - '0' : num1[i] - 'A' + 10) +
        (isdigit(num2[i]) ? num2[i] - '0' : num2[i] - 'A' + 10);
        if (num >= base)
        {
            num = num - base;
            num1[i] = (num <= 9 ? num + '0' : num - 10 + 'A');
            num1[i - 1] += 1;
        }
        else
        {
            num1[i] = (num <= 9 ? num + '0' : num - 10 + 'A');
        }
    }
    return num1;
}
std::string sum_in_base(int num, int base, ...)
{
    va_list list;
    va_start(list, base);
    std::string result = va_arg(list, std::string);
    for (int i = 0; i < num - 1; i++)
    {
        std::string num = va_arg(list, std::string);
        result = sum(num, result, base);
    }
    va_end(list);
    while(true)
    {
        if (result[0] == '0' && result.size() > 1)
        {
            result.erase(0, 1);
        }
        else
            break;
    }
    return result;
}


int main()
{
    std::cout << "Student: Ivanchenko Darya\nGroup: M8O-210B-20\n";
    std::string res = sum_in_base(2, 16, (std::string)"0", (std::string)"0");
    std::cout << res << '\n';
    return 0;
}
