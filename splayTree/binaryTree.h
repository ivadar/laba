#ifndef BINARYTREE_H
#define BINARYTREE_H

#include <iostream>
#include <exception>
#include <functional>
#include <vector>
#include <stack>

enum status_t
{
    FIND_SUCCESS,
    FIND_ERROR,
    INSERT_SUCCESS,
    INSERT_ERROR,
    REMOVE_SUCCESS,
    REMOVE_ERROR
};

template <typename T>

class Comparator
{
    public:
        virtual int compare(const T& object_1, const T& object_2) const = 0;
        virtual ~Comparator() = default;
};

template <typename T>

class Default_comparator: public Comparator<T>
{
    int compare(const T& object_1, const T& object_2) const
    {
        if (object_1 > object_2)
        {
            return 1;
        }
        if (object_1 < object_2)
        {
            return -1;
        }
        return 0;
    }
};

template <typename TKey, typename TValue>

struct Node
{
    TKey key;
    TValue value;
    int height;
    Node(const TKey &k, const TValue &v)
    {
        key = k;
        value = v;
        height = 1;
    }
    Node *left = nullptr;
    Node *right = nullptr;
};


template <typename TKey, typename TValue>

class Container
{
    public:
        virtual void insert(const Node<TKey, TValue>& object) = 0;
        virtual TValue find(const TKey& key) = 0;
        virtual TValue remove(const TKey& key) = 0;
        virtual ~Container() = default;
};


template <typename TKey, typename TValue>

class BinaryTree: public Container<TKey, TValue>
{
    public:

        class TreeException: public std::exception
        {
            public:
            virtual const char* what() const noexcept = 0;
        };
        class ItemExist: public TreeException
        {
            public:
            TKey key;
            ItemExist(const TKey &_key)
            {
                key = _key;
            }
            const char* what() const noexcept
            {
                std::cout << "Key: " << key ;
                return "Item already has existed\n";
            }
        };
        class ItemNoExist: public TreeException
        {
            public:
            TKey key;
            ItemNoExist(const TKey &_key): key { _key }
            {

            }
            const char* what() const noexcept
            {
                std::cout << "Key: " << key;
                return "Item is not exists\n";
            }
        };
        class InvalidKey: public TreeException
        {
            public:
            TKey key;
            InvalidKey(const TKey &_key): key { _key }
            {

            }
            const char* what() const noexcept
            {
                std::cout << "Key: " << key;
                return "Invalid key\n";
            }
        };

    protected:
        class InsertTemplate
        {
            public:
                status_t invokeInsert(Node<TKey, TValue> *& root,
                                        const TKey &key, const TValue &value,
                                        Comparator<TKey> *keyComparator, std::stack<Node<TKey, TValue> *>& stackNode);
            protected:
                virtual void insertHook(Node<TKey, TValue> *& root,
                                        const TKey &key, const TValue &value,
                                        Comparator<TKey> *keyComparator, std::stack<Node<TKey, TValue> *>& stackNode)
                {

                }
        };
        class FindTemplate
        {
            public:
                status_t invokeFind(Node<TKey, TValue> *& root,
                                        const TKey &key, TValue &value, Comparator<TKey> *keyComparator,
                                        std::stack<Node<TKey, TValue> *>& stackNode);
            protected:
                virtual void findHook(Node<TKey, TValue> *& root,
                                        const TKey &key, const TValue &value, Comparator<TKey> *keyComparator,
                                        std::stack<Node<TKey, TValue> *>& stackNode)
                {

                }
        };
        class RemoveTemplate
        {
            public:
                status_t invokeRemove(Node<TKey, TValue> *& root,
                                        const TKey &key, TValue &value,
                                        Comparator<TKey> *keyComparator, std::stack<Node<TKey, TValue> *>& stackNode);
            protected:
                virtual void removeHook(Node<TKey, TValue> *& root,
                                        const TKey &key, const TValue &value,
                                        Comparator<TKey> *keyComparator, std::stack<Node<TKey, TValue> *>& stackNode)
                {

                }
                virtual void removenodeHook(Node<TKey, TValue> *&current, Node<TKey, TValue> *&parent,
                                            TValue &value, std::stack<Node<TKey, TValue> *>& stackNode);
        };

    public:
        BinaryTree()
        {

        }
        BinaryTree(InsertTemplate *_inserter, FindTemplate *_finder,
                    RemoveTemplate *_remover, Comparator<TKey> *compare = nullptr):
                        inserter { _inserter }, finder { _finder },
                        remover { _remover }, comparator { compare }
        {

        }
        BinaryTree(Comparator<TKey> *compare)
        {
            inserter = new InsertTemplate;
            finder = new FindTemplate;
            remover = new RemoveTemplate;
            comparator = compare;
        }
        BinaryTree(const BinaryTree<TKey, TValue>& object);
        ~BinaryTree();
        BinaryTree<TKey, TValue>& operator=(const BinaryTree<TKey, TValue>& object);

        typedef std::function<void(const TKey &k, const TValue &value, int depth)> callbackFunction;

        void insert(const Node<TKey, TValue>& object);
        TValue find(const TKey& key);
        TValue remove(const TKey& key);

        void prefix(callbackFunction) const;
        void postfix(callbackFunction) const;
        void infix(callbackFunction) const;
        void go_prefix(const Node<TKey, TValue>*, callbackFunction, int _depth) const;
        void go_postfix(const Node<TKey, TValue>*, callbackFunction, int _depth) const;
        void go_infix(const Node<TKey, TValue>*, callbackFunction, int _depth) const;


    protected:
        Node<TKey, TValue> *root = nullptr; // корень дерева
        Comparator<TKey> *comparator = nullptr; // компаратор
        InsertTemplate *inserter = nullptr;
        FindTemplate *finder = nullptr;
        RemoveTemplate *remover = nullptr;
};

template <typename TKey, typename TValue>

void BinaryTree<TKey, TValue>::prefix(callbackFunction func) const
{
    go_prefix(root, func, 0);
}

template <typename TKey, typename TValue>

void BinaryTree<TKey, TValue>::go_prefix(const Node<TKey, TValue>* root, callbackFunction func, int _depth) const
{
    if (root != nullptr)
    {
        func(root->key, root->value, _depth);
        if (root->left)
        {
            go_prefix(root->left, func, _depth + 1);
        }
        if (root->right)
        {
            go_prefix(root->right, func, _depth + 1);
        }
    }
}

template <typename TKey, typename TValue>

void BinaryTree<TKey, TValue>::postfix(callbackFunction func) const
{
    go_postfix(root, func, 0);
}

template <typename TKey, typename TValue>

void BinaryTree<TKey, TValue>::go_postfix(const Node<TKey, TValue>* root, callbackFunction func, int _depth) const
{
    if (root != nullptr)
    {
        if (root->left)
        {
            go_postfix(root->left, func, _depth + 1);
        }
        if (root->right)
        {
            go_postfix(root->right, func, _depth + 1);
        }
        func(root->key, root->value, _depth);
    }
}

template <typename TKey, typename TValue>

void BinaryTree<TKey, TValue>::infix(callbackFunction func) const
{
    go_infix(root, func, 0);
}

template <typename TKey, typename TValue>

void BinaryTree<TKey, TValue>::go_infix(const Node<TKey, TValue>* root, callbackFunction func, int _depth) const
{
    if (root != nullptr)
    {
        if (root->left)
        {
            go_infix(root->left, func, _depth + 1);
        }
        func(root->key, root->value, _depth);
        if (root->right)
        {
            go_infix(root->right, func, _depth + 1);
        }
    }
}

template <typename TKey, typename TValue>

BinaryTree<TKey, TValue>::BinaryTree(const BinaryTree<TKey, TValue>& object):
                inserter{ object.inserter }, finder { object.finder },
                remover { object.remover}, comparator { object.comparator}
{
    std::vector<Node<TKey, TValue>> newTree;
    prefix([&newTree](TKey key, TValue value, int depth) { newTree.insert_back({key, value}); } );
    for (int i = 0; i < newTree.size(); i++)
    {
        this->insert(newTree[i]);
    }
}

template <typename TKey, typename TValue>

BinaryTree<TKey, TValue>& BinaryTree<TKey, TValue>::operator=(const BinaryTree<TKey, TValue>& object)
{
    this->inserter = object.inserter;
    this->finder = object.finder;
    this->remover = object.remover;
    this->comparator = object.comparator;
    std::vector<Node<TKey, TValue>> newTree;
    prefix([&newTree](TKey &key, TValue &value, int depth) { newTree.insert_back({key, value}); } );
    for (int i = 0; i < newTree.size(); i++)
    {
        this->insert(newTree[i]);
    }
    return *this;
}

template <typename TKey, typename TValue>

status_t BinaryTree<TKey, TValue>::FindTemplate::invokeFind(Node<TKey, TValue> *& root,
                                        const TKey &key, TValue &value, Comparator<TKey> *keyComparator,
                                        std::stack<Node<TKey, TValue> *>& stackNode)
{
    Node<TKey, TValue> *current = root;
    while (current)
    {
        stackNode.push(current);
        switch (keyComparator->compare(key, current->key))
        {
        case -1:
            current = current->left;
            break;
        case 1:
            current = current->right;
            break;
        case 0:
            value = current->value;
            findHook(root, key, value, keyComparator, stackNode);
            return FIND_SUCCESS;
        default:
            break;
        }
    }
    return FIND_ERROR;
}

template <typename TKey, typename TValue>

status_t BinaryTree<TKey, TValue>::InsertTemplate::invokeInsert(Node<TKey, TValue> *&root,
                                        const TKey &key, const TValue &value, Comparator<TKey> *keyComparator,
                                        std::stack<Node<TKey, TValue> *>& stackNode)
{
    Node<TKey, TValue> *insertNode = new Node<TKey, TValue>(key, value);
    if (!root)
    {
        root = insertNode;
        stackNode.push(root);
        insertHook(root, key, value, keyComparator, stackNode);
        return INSERT_SUCCESS;
    }
    else
    {
        Node<TKey, TValue> *current = root;
        int last = 0;
        while (current)
        {
            last = keyComparator->compare(key, current->key);
            stackNode.push(current);
            if (last < 0)
            {
                if (current->left)
                {
                    current = current->left;
                    continue;
                }
                break;
            }
            else if (last > 0)
            {
                if (current->right)
                {
                    current = current->right;
                    continue;
                }
                break;
            }
            else if (last == 0)
            {
                delete insertNode;
                return INSERT_ERROR;
            }
        }
        if (last < 0)
        {
            current->left = insertNode;
            stackNode.push(current->left);
        }
        else if (last > 0)
        {
            current->right = insertNode;
            stackNode.push(current->right);
        }
    }
    insertHook(root, key, value, keyComparator, stackNode);
    return INSERT_SUCCESS;
}

template <typename TKey, typename TValue>

status_t BinaryTree<TKey, TValue>::RemoveTemplate::invokeRemove(Node<TKey, TValue> *& root,
                                        const TKey &key, TValue &value, Comparator<TKey> *keyComparator,
                                        std::stack<Node<TKey, TValue> *>& stackNode)
{
    if (!root)
    {
        return REMOVE_ERROR;
    }
    else if (!root->left && !root->right)
    {
        value = root->value;
        delete root;
        root = nullptr;
        return REMOVE_SUCCESS;
    }
    else
    {
        Node<TKey, TValue> *parent = root;
        Node<TKey, TValue> *current = root;
        while (int comparer = keyComparator->compare(key, current->key))
        {
            stackNode.push(current);
            if (comparer < 0)
            {
                if (current->left)
                {
                    parent = current;
                    current = current->left;
                }
                else
                {
                    return REMOVE_ERROR;
                }

            }
            else if (comparer > 0)
            {
                if (current->right)
                {
                    parent = current;
                    current = current->right;
                }
                else
                {
                    return REMOVE_ERROR;
                }
            }
        }
        removenodeHook(current, parent, value, stackNode);
    }
    removeHook(root, key, value, keyComparator, stackNode);
    return REMOVE_SUCCESS;
}
template <typename TKey, typename TValue>

void BinaryTree<TKey, TValue>::RemoveTemplate::removenodeHook(Node<TKey, TValue> *&current, Node<TKey, TValue> *&parent, TValue &value, std::stack<Node<TKey, TValue> *>& stackNode)
{
    if (current->left && current->right)
    {
        Node<TKey, TValue> *replaceNode = current->right;
        Node<TKey, TValue> *replaceParentNode = current;
        while (replaceNode)
        {
            if (replaceNode->left)
            {
                replaceParentNode = replaceNode;
                replaceNode = replaceNode->left;
                stackNode.push(replaceParentNode);
            }
            else
                break;
        }
        current->key = replaceNode->key;
        current->value = replaceNode->value;
        if (replaceNode->right)
        {
            if (replaceNode == replaceParentNode->left)
            {
                replaceParentNode->left = replaceNode->right;
            }
            else
            {
                replaceParentNode->right = replaceNode->right;
            }
        }
        else
        {
            if (replaceNode == replaceParentNode->left)
            {
                replaceParentNode->left = nullptr;
            }
            else
            {
                replaceParentNode->right = nullptr;
            }
        }
        value = replaceNode->value;
        delete replaceNode;
        replaceNode = nullptr;
    }
    else if (current->left)
    {
        if (parent->left == current)
            parent->left = current->left;
        else
            parent->right = current->left;
        value = current->value;
        delete current;
        current = nullptr;
    }
    else if (current->right)
    {
        if (parent->left == current)
            parent->left = current->right;
        else
            parent->right = current->right;
        parent->right = current->right;
        value = current->value;
        delete current;
        current = nullptr;
    }
    else
    {
        if (parent->left == current)
        {
            parent->left = nullptr;
        }
        else
        {
            parent->right = nullptr;
        }
        value = current->value;
        delete current;
        current = nullptr;
    }
}

template <typename TKey, typename TValue>

void BinaryTree<TKey, TValue>::insert(const Node<TKey, TValue>& object)
{
    std::stack<Node<TKey, TValue> *> stackNodes;
    status_t status = inserter->invokeInsert(this->root, object.key, object.value, this->comparator, stackNodes);
    if (status == INSERT_ERROR)
    {
        throw ItemExist(object.key);
    }
}

template <typename TKey, typename TValue>

TValue BinaryTree<TKey, TValue>::find(const TKey& key)
{
    TValue value;
    std::stack<Node<TKey, TValue> *> stackNodes;
    status_t status = finder->invokeFind(this->root, key, value, this->comparator, stackNodes);
    if (status == FIND_ERROR)
    {
        throw InvalidKey(key);
    }
    return value;
}

template <typename TKey, typename TValue>

TValue BinaryTree<TKey, TValue>::remove(const TKey& key)
{
    TValue value;
    std::stack<Node<TKey, TValue> *> stackNodes;
    status_t status = remover->invokeRemove(this->root, key, value, this->comparator, stackNodes);
    if (status == REMOVE_ERROR)
    {
        throw ItemNoExist(key);
    }
    return value;
}

template <typename TKey, typename TValue>

BinaryTree<TKey, TValue>::~BinaryTree()
{
    std::vector<Node<TKey, TValue>> deleteTree;
    this->postfix([&deleteTree](TKey key, TValue value, int depth) { deleteTree.push_back({key, value}); });
    for(int i = 0; i < deleteTree.size(); i++)
    {
        remove(deleteTree[i].key);
    }
    delete this->finder;
    delete this->remover;
    delete this->inserter;
}

template <typename Key, typename Value>

void print_result(const Key &key, const Value &value, int depth)
{
    std::cout << "Key = " << key << "; " << "Value = " << value << "; " << "Depth = "<< depth << ";" <<std::endl;
}
#endif // BINARYTREE_H
