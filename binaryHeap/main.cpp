#include "binaryHeap.h"

int main()
{
    Comparator<int> *comparator_int = new Default_comparator<int>;
        CreatorBinaryHeap<int, int> *heap_creator = new CreatorBinaryHeap<int, int>;
        BinaryHeap<int, int> *example = static_cast<BinaryHeap<int, int> *>(heap_creator->FactoryMethod(comparator_int));
        BinaryHeap<int, int> *example_1 = static_cast<BinaryHeap<int, int> *>(heap_creator->FactoryMethod(comparator_int));
        cout << "Binary heap name: example" << endl;
        try
        {
           int value;
           example->getMin(value);
           cout << "Get minimum : " << value;
        }
        catch (HeapException &exception)
        {
            cout << exception.what() << endl;
        }
        try
        {
            cout << "Insert 9 : 9" << endl;
            example->insertValue(9, 9);
            cout << "Insert 1 : 1" << endl;
            example->insertValue(1, 1);
            cout << "Insert 7 : 7" << endl;
            example->insertValue(7, 7);
            cout << "Insert 6 : 6" << endl;
            example->insertValue(6, 6);
        }
        catch (HeapException &exception)
        {
            cout << exception.what() << endl;
        }
        try
        {
           int value;
           example->getMin(value);
           cout << "Get minimum : " << value << '\n';
        }
        catch (HeapException &exception)
        {
            cout << exception.what() << endl;
        }
        try
        {
           cout << "Remove minimum" << endl;
           example->removeMinKey();
        }
        catch (HeapException &exception)
        {
            cout << exception.what() << endl;
        }
        try
        {
           int value;
           example->getMin(value);
           cout << "Get minimum : " << value << '\n';
        }
        catch (HeapException &exception)
        {
            cout << exception.what() << endl;
        }
        cout << "Binary heap name: example_1" << endl;
        try
        {
            cout << "Insert -9 : -9" << endl;
            example_1->insertValue(-9, -9);
            cout << "Insert -1 : -1" << endl;
            example_1->insertValue(-1, -1);
            cout << "Insert -7 : -7" << endl;
            example_1->insertValue(-7, -7);
            cout << "Insert -6 : -6" << endl;
            example_1->insertValue(-6, -6);
        }
        catch (HeapException &exception)
        {
            cout << exception.what() << endl;
        }
        try
        {
           cout << "Merge heap example with example_1" << endl;
           example->merge(*example_1);
        }
        catch (HeapException &exception)
        {
            cout << exception.what() << endl;
        }
        try
        {
           int value;
           example->getMin(value);
           cout << "Get minimum : " << value << '\n';
        }
        catch (HeapException &exception)
        {
            cout << exception.what() << endl;
        }
        delete comparator_int;
        delete heap_creator;
        delete example;
        delete example_1;
}
